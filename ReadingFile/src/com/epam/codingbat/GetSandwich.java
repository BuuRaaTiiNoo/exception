package com.epam.codingbat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//A sandwich is two pieces of bread with something in between. Return the string that is between the
//first and last appearance of
//"bread" in the given string, or return the empty string "" if there are not two pieces of bread.
public class GetSandwich {
    private static String getSandwich(String str) {
        Pattern p = Pattern.compile("bread(.*?)bread");
        Matcher m = p.matcher(str);
        StringBuilder s = new StringBuilder();
        while (m.find())
            s.append(m.group(1));
        return s.toString();
    }

    public static void main(String[] args) {
        System.out.println(getSandwich("qwesadbreadjambreadadfdgfg"));
    }
}
