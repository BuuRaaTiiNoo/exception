package com.epam.codingbat;

//Given an array of ints, return true if the array contains two 7's next to each other,
// or there are two 7's separated by one element, such as with {7, 1, 7}.
public class HasSeven {
    private static boolean has77(int[] nums) {
        int j = 0;
        int k = 0;
        for(int i = 1; i < nums.length; i++){
            if (nums[j] == nums[i] && nums[i] == 7)
                return true;
            j++;
        }
        for (int m = 2; m < nums.length; m++){
            if(nums[k] == nums[m] && nums[m] == 7)
                return true;
            k++;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(has77(new int[]{1,2,3,4,5,6,7}));
    }
}
