package com.epam.codingbat;

//Return true if the array contains, somewhere, three increasing adjacent numbers like .... 4, 5, 6, ... or 23, 24, 25.
public class TripleUp {
    public boolean tripleUp(int[] nums) {
        int j = 1;
        int k = 2;
        for (int i = 0; i < nums.length - 2; i++){
            if(nums.length >= 3)
                if (nums[i] == nums[j] - 1 && nums[i] == nums[k] - 2)
                    return true;
            j++;
            k++;
        }
        return false;
    }

}
